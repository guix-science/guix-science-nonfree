;;;
;;; Copyright © 2022-2025 Emmanuel Medernach <Emmanuel.Medernach@iphc.cnrs.fr>
;;; Copyright © 2023-2025 Jake Forster <jakecameron.forster@gmail.com>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (guix-science-nonfree packages geant4)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module ((guix-science-nonfree licenses) #:prefix nonfree:)
  #:use-module (guix-science packages physics)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages xml)
  #:use-module (guix build-system cmake)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages fontutils))


;;; Geant4 datasets
;;;
(define (geant4-dataset name version hash)
  (origin
    (method url-fetch)
    (uri (string-append "https://cern.ch/geant4-data/datasets/" name "."
                        version ".tar.gz"))
    (sha256 (base32 hash))))

;; G4NDL
;;
(define g4ndl-4.7
  (geant4-dataset "G4NDL" "4.7"
                  "0283cwylajyjm4267ngfc2bd3452623r5bakywaccb8h44k3szby"))

(define g4ndl-4.7.1
  (geant4-dataset "G4NDL" "4.7.1"
                  "0z3clj2i9pr42y7jipd91nzic97v6gam8jp2kmbx4611c94axb6k"))

;; G4EMLOW
;;
(define g4emlow-8.2
  (geant4-dataset "G4EMLOW" "8.2"
                  "09z4m3hq6895s7vwiaham7zbfq0ww6xh8xh8jv5kp9gm9wk6hxrx"))

(define g4emlow-8.5
  (geant4-dataset "G4EMLOW" "8.5"
                  "0wzgpklx776f14crhriyh08ya9b24vxv89f122nf4iaxmi4wmfk6"))

(define g4emlow-8.6.1
  (geant4-dataset "G4EMLOW" "8.6.1"
                  "12jaiz6xc8shg4aj9fhwizxhajdyvbx7ckxr6v9y23084s6mi4sa"))

;; PhotonEvaporation
;;
(define photon-evaporation-5.7
  (geant4-dataset "G4PhotonEvaporation" "5.7"
                  "1rg7fygfxx06h98ywlci6b0b9ih74q8diygr76c3vppxdzjl47kn"))

(define photon-evaporation-6.1
  (geant4-dataset "G4PhotonEvaporation" "6.1"
                  "1wj6n8xj521i5fwb6hhq9ji3pibkmxs9imc6041cjl0xm2cizz2z"))

;; RadioactiveDecay
;;
(define radioactive-decay-5.6
  (geant4-dataset "G4RadioactiveDecay" "5.6"
                  "1w8d9zzc4ss7sh1f8cxv5pmrx2b74p1y26377rw9hnlfkiy0g1iq"))

(define radioactive-decay-6.1.2
  (geant4-dataset "G4RadioactiveDecay" "6.1.2"
                  "0fm1mr6934qj299x0m23v02xcp4lw3qhz7d4qiamblv4phz7w3d4"))

;; G4PARTICLEXS
;;
(define g4particlexs-4.0
  (geant4-dataset "G4PARTICLEXS" "4.0"
                  "15fa6c8jh6g3nj82ychc13wlz2rc58v9jjdb6vyv1wn30fbh70ck"))

(define g4particlexs-4.1
  (geant4-dataset "G4PARTICLEXS" "4.1"
                  "1lfplq9354ly1cdaz5rdhb60rmavsmylk5k63zwygj4siq21xbh7"))

;; G4PII
;;
(define g4pii-1.3
  (geant4-dataset "G4PII" "1.3"
                  "09p92rk1sj837m6n6yd9k9a8gkh6bby2bfn6k0f3ix3m4s8as9b2"))

;; RealSurface
;;
(define real-surface-2.2
  (geant4-dataset "G4RealSurface" "2.2"
                  "08382y1258ifs7nap6zaaazvabg72blr0dkqgwk32lrg07hdwm4r"))

;; G4SAIDDATA
;;
(define g4saiddata-2.0
  (geant4-dataset "G4SAIDDATA" "2.0"
                  "149fqy801n1pj2g6lcai2ziyvdz8cxdgbfarax6y8wdakgksh9hx"))

;; G4ABLA
;;
(define g4abla-3.1
  (geant4-dataset "G4ABLA" "3.1"
                  "1v97q28g1xqwnav0lwzwk7hc3b87yrmbvkgadf4bkwcbnm9b163n"))

(define g4abla-3.3
  (geant4-dataset "G4ABLA" "3.3"
                  "1cd25vckckxkhyx3pvz5swral0rkd4z7akv2dn4fz77fa8r1n10y"))

;; G4INCL
;;
(define g4incl-1.0
  (geant4-dataset "G4INCL" "1.0"
                  "0z9nqk125vvf4f19lhgb37jy60jf9zrjqg5zbxbd1wz93a162qbi"))

(define g4incl-1.2
  (geant4-dataset "G4INCL" "1.2"
                  "0zhs1vnrc0vhb1y4q3bscz9y2k9dsnk7ccjg97br42pffdhb307q"))

;; G4ENSDFSTATE
;;
(define g4ensdfstate-2.3
  (geant4-dataset "G4ENSDFSTATE" "2.3"
                  "00wjir59rrrlk0a12vi8rsnhyya71rdi1kmark9sp487hbhcai4l"))

(define g4ensdfstate-3.0
  (geant4-dataset "G4ENSDFSTATE" "3.0"
                  "1mm7xfjcjncwd1fdhknnaxjhlm2yf1az11sgpy2k9m1i1ga3pp2b"))

;; G4CHANNELING
;;
(define g4channeling-1.0
  (geant4-dataset "G4CHANNELING" "1.0"
                  "1wk71l99brh51f7n0v1fq55svbzgn2lk278s336rm82ck1lkqgi0"))

;; G4TENDL
;;
(define g4tendl-1.4
  (geant4-dataset "G4TENDL" "1.4"
                  "1q11jxfy5kjwb0jrvwv6dgdxr3h85s6g2bl9kdbfvd681h178wjb"))

;; G4NUDEXLIB
;;
(define g4nudexlib-1.0
  (geant4-dataset "G4NUDEXLIB" "1.0"
                  "05gljmiwhk6yfrg80py9cn8gkahn5s1darr65fxfvy2skigddiya"))

;; G4URRPT
;;
(define g4urrpt-1.1
  (geant4-dataset "G4URRPT" "1.1"
                  "04cw1nr0lh2i0j3i9sjpcdfh04s92b0bj16537p8l25wh3dk4d3a"))


;;; Geant4
;;;
;;; The Geant4 license has an anti-patent clause that might make it
;;; nonfree.
(define-public geant4-11.1
  (package
    (name "geant4")
    (version "11.1.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.cern.ch/geant4/geant4")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0w616j4w8gmmn5815pnyylm9sv0gpdicpjbvxwpyhzqlssmm2bjl"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:configure-flags
      #~(list "-DGEANT4_USE_GDML=ON"
              "-DGEANT4_USE_SYSTEM_CLHEP=ON"
              (string-append "-DGEANT4_INSTALL_DATADIR="
                             #$output "/share/geant4/data"))
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'install 'install-data
            (lambda _
              (let ((datadir (string-append #$output
                                            "/share/geant4/data")))
                (mkdir-p datadir)
                (for-each
                 (lambda (archive)
                   (invoke "tar" "xvf" archive "-C" datadir))
                 (map cadr '#$(package-native-inputs this-package)))))))
      #:tests? #f))
    (inputs (list clhep-2.4.6.2 expat xerces-c)) ;xerces-c is for GDML
    (native-inputs
     (list g4ndl-4.7
           g4emlow-8.2
           photon-evaporation-5.7
           radioactive-decay-5.6
           g4particlexs-4.0
           g4pii-1.3
           real-surface-2.2
           g4saiddata-2.0
           g4abla-3.1
           g4incl-1.0
           g4ensdfstate-2.3
           g4tendl-1.4))
    (home-page "https://geant4.web.cern.ch")
    (synopsis "Monte Carlo particle track simulations")
    (description
     "Geant4 is a toolkit for the simulation of the passage of
particles through matter.  Its areas of application include high
energy, nuclear and accelerator physics, as well as studies in medical
and space science.")
    (license (nonfree:nonfree
              "https://geant4.web.cern.ch/download/license"))))

(define-public geant4-vis-11.1
  (package
    (inherit geant4-11.1)
    (name "geant4-vis")
    (arguments
     (substitute-keyword-arguments (package-arguments geant4-11.1)
       ((#:configure-flags flags)
        #~(append
           (list "-DGEANT4_USE_QT=ON" "-DGEANT4_USE_FREETYPE=ON")
           #$flags))))
    (inputs (modify-inputs (package-inputs geant4-11.1)
              (append qtbase-5 freetype)))
    (synopsis
     "Monte Carlo particle track simulations (visualization variant)")))

(define-public geant4-11.2
  (package
    (inherit geant4-11.1)
    (version "11.2.2")
    (source
     (origin
       (inherit (package-source geant4-11.1))
       (uri (git-reference
             (url "https://gitlab.cern.ch/geant4/geant4")
             (commit (string-append "v" version))))
       (sha256
        (base32 "111v65zx09fyddnqv1d0b9x15wqhyl0548ny2ysv4x66wyrp8p9r"))))
    (inputs (modify-inputs (package-inputs geant4-11.1)
              (replace "clhep" clhep-2.4.7.1)))
    (native-inputs
     (list g4ndl-4.7.1
           g4emlow-8.5
           photon-evaporation-5.7
           radioactive-decay-5.6
           g4particlexs-4.0
           g4pii-1.3
           real-surface-2.2
           g4saiddata-2.0
           g4abla-3.3
           g4incl-1.2
           g4ensdfstate-2.3
           g4tendl-1.4))))

(define-public geant4-vis-11.2
  (package
    (inherit geant4-11.2)
    (name "geant4-vis")
    (arguments
     (substitute-keyword-arguments (package-arguments geant4-11.2)
       ((#:configure-flags flags)
        #~(append
           (list "-DGEANT4_USE_QT=ON" "-DGEANT4_USE_FREETYPE=ON")
           #$flags))))
    (inputs (modify-inputs (package-inputs geant4-11.2)
              (append qtbase-5 freetype)))
    (synopsis
     "Monte Carlo particle track simulations (visualization variant)")))

(define-public geant4-11.3
  (package
    (inherit geant4-11.2)
    (version "11.3.0")
    (source
     (origin
       (inherit (package-source geant4-11.2))
       (uri (git-reference
             (url "https://gitlab.cern.ch/geant4/geant4")
             (commit (string-append "v" version))))
       (sha256
        (base32 "15wmpnwnm640j71z17fld8p43x5axdgmgvxb4xfzk3yqw4yyi1nf"))))
    (arguments
     (list
      #:configure-flags
      #~(list "-DGEANT4_USE_GDML=ON"
              "-DGEANT4_USE_SYSTEM_CLHEP=ON"
              (string-append "-DGEANT4_INSTALL_DATADIR="
                             #$output "/share/geant4/data")
              ;; To build Geant4 user applications without propagating
              ;; this package's inputs.
              "-DGEANT4_INSTALL_PACKAGE_CACHE=ON")
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'install 'install-data
            (lambda _
              (let ((datadir (string-append #$output
                                            "/share/geant4/data")))
                (mkdir-p datadir)
                (for-each
                 (lambda (archive)
                   (invoke "tar" "xvf" archive "-C" datadir))
                 (map cadr '#$(package-native-inputs this-package)))))))
      #:tests? #f))
    (native-inputs
     (list g4ndl-4.7.1
           g4emlow-8.6.1
           photon-evaporation-6.1
           radioactive-decay-6.1.2
           g4particlexs-4.1
           g4pii-1.3
           real-surface-2.2
           g4saiddata-2.0
           g4abla-3.3
           g4incl-1.2
           g4ensdfstate-3.0
           g4channeling-1.0
           g4tendl-1.4
           g4nudexlib-1.0
           g4urrpt-1.1))))

(define-public geant4-vis-11.3
  (package
    (inherit geant4-11.3)
    (name "geant4-vis")
    (arguments
     (substitute-keyword-arguments (package-arguments geant4-11.3)
       ((#:configure-flags flags)
        #~(append
           (list "-DGEANT4_USE_QT=ON" "-DGEANT4_USE_QT_QT6=ON"
                 "-DGEANT4_USE_FREETYPE=ON")
           #$flags))))
    (inputs (modify-inputs (package-inputs geant4-11.3)
              (append qtbase freetype)))
    (synopsis
     "Monte Carlo particle track simulations (visualization variant)")))

(define-public geant4 geant4-11.3)

(define-public geant4-single-threaded
  (package/inherit geant4
    (name (string-append (package-name geant4) "-single-threaded"))
    (arguments (substitute-keyword-arguments (package-arguments geant4)
       ((#:configure-flags flags)
        #~(append #$flags
                  (list "-DGEANT4_BUILD_MULTITHREADED=OFF")))))
    (synopsis
     "Monte Carlo particle track simulations (single-threaded variant)")))
