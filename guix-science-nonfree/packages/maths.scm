;;; Copyright © 2025 Lars Bilke <lars.bilke@ufz.de>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (guix-science-nonfree packages maths)
  #:use-module (guix)
  #:use-module (gnu packages maths)
  #:use-module (guix-science-nonfree packages mkl))

(define-public petsc-openmpi-mkl
  (package
    (inherit petsc-openmpi)
    (name "petsc-openmpi-mkl")
    (inputs (modify-inputs (package-inputs petsc-openmpi)
              (prepend intel-oneapi-mkl)))
    (arguments
     (substitute-keyword-arguments (package-arguments petsc-openmpi)
       ((#:configure-flags _)
        #~(list "--with-openmp=1"
                "--with-openblas=0"
                "--with-superlu=1"
                "--with-debugging=0"
                "--with-hypre=1"
                "--with-mpiexec=mpirun"
                "--with-metis=1"
                "--with-mumps=1"
                "--with-scalapack=1"
                "--with-ptscotch=1"
                (string-append "--with-mpi-dir="
                               #$(this-package-input "openmpi"))
                (string-append "--with-hdf5-include="
                               #$(this-package-input "hdf5-parallel-openmpi")
                               "/include")
                (string-append "--with-hdf5-lib="
                               #$(this-package-input "hdf5-parallel-openmpi")
                               "/lib/libhdf5.a")
                (string-append "--with-mkl_pardiso-dir="
                               #$(this-package-input "intel-oneapi-mkl"))
                (string-append "--with-mkl_cpardiso-dir="
                               #$(this-package-input "intel-oneapi-mkl"))
                (string-append "--with-blaslapack-dir="
                               #$(this-package-input "intel-oneapi-mkl"))
                               ))))
    (synopsis "Library to solve PDEs (with MUMPS, MPI and MKL support)")))
