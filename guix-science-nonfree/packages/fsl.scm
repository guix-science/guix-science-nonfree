;;; Copyright © 2025 Julien Castelneau <julien.castelneau@inria.fr>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-science-nonfree packages fsl)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (guix build utils)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
  #:use-module (guix-science-nonfree licenses)
  #:use-module (guix-science-nonfree packages cuda)
  #:use-module (gnu packages)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages base)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gd)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages image)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages python)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages xml)
  #:use-module ((guix licenses) #:prefix license:))

(define-public ciftilib
  (package
    (name "ciftilib")
    (version "1.6.0")
    (home-page "https://github.com/Washington-University/CiftiLib")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0h0gh2dwnxkhkjwilkjkm8n7bf0asxxc6zs5c0rjmq8yrfj9vkf5"))))
    (build-system cmake-build-system)
    (inputs (list qtbase-5
                  glib
                  glibmm
                  libxml++
                  libxml2
                  libsigc++
                  gtkmm
                  boost))
    (synopsis "C++ Library for reading and writing CIFTI-2 and CIFTI-1 files")
    (description "CiftiLib is a library for the CIFTI file format,
as documented here: http://www.nitrc.org/projects/cifti/.
CIFTI (Connectivity Informatics Technology Initiative) standardizes file formats
for the storage of connectivity data.  These formats are developed by the Human
Connectome Project and other interested parties.")
    (license license:bsd-2)))

(define fsl-license
  (nonfree "https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Licence"
     "This is a non-free license for non-commercial use only."))

(define fsl-base
  (package
    (name "fsl-base")
    (version "2411.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/base")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1m79429zn3k9g2babjnd6dgkmw4c28jrxqjw6vyrw7nlwb8x8sfx"))))
    (build-system gnu-build-system)
    (native-inputs (list python))
    (arguments
     (list #:tests? #f ;; no tests in package.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-after 'unpack 'fix-path
                 (lambda _
                   (substitute* "Makefile"
                     (("python -m pip install" motif) (string-append "echo " motif))
                     (("/bin/mkdir") "mkdir"))
                   (substitute* "./config/buildSettings.mk"
                     (("/bin/sh") "sh")
                     (("/bin/rm") "rm")
                     (("/bin/cp") "cp")
                     (("/bin/mv") "mv")
                     (("/bin/chmod") "chmod")
                     (("/bin/mkdir") "mkdir"))
                   (substitute* "./config/rules.mk"
                     (("ln -s " motif) (string-append motif #$output "/bin/")))
                   (setenv "PREFIX" #$output))))))
    (synopsis "Build dependency for FSL")
    (description "This package is a build dependency of all FSL modules.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/base")
    (license fsl-license)))

(define-public fsl-utils
  (package
    (name "fsl-utils")
    (version "2203.5")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/utils")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1pw95xgb6qzq0cln82nr2im0ra5asfys353160vyg5w4wr6gnizr"))))
    (inputs (list fsl-armawrap
                  openblas
                  lapack
                  zlib))
    (native-inputs (list fsl-base))
    (arguments
     (list #:tests? #f ; No tests in package.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Set of utilities libraries for FSL")
    (description "This package is a set of utilities libraries all FSL modules")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/utils")
    (license fsl-license)))

(define-public fsl-armawrap
  (package
    (name "fsl-armawrap")
    (version "0.6.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/armawrap")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "07d35j9zqk34h0rdw2m6mljpyp420235makx8rs2bb2jq17yrpi0"))))
    (native-inputs (list fsl-base))
    (inputs (list lapack))
    (arguments
     (list
      #:tests? #f ; No check target in Makefile.
      #:phases
      #~(modify-phases %standard-phases
          (delete 'configure)
          (add-before 'build 'extra-env
            (lambda _
              (setenv "FSLCONFDIR"
                      (string-append
                       #$(this-package-native-input "fsl-base") "/config"))
              (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Wrapper around Armadillo which provides a Newmat style API")
    (description "A newmat-like wrapper around the Armadillo linear algebra library.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/armawrap")
    (license license:asl2.0)))

(define-public fsl-znzlib
  (package
    (name "fsl-znzlib")
    (version "2111.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/znzlib")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1rjg1rpbq56mqsf11zyg9yggd0vqigc4knz80ngdx9mvzp2p2d3i"))))
    (native-inputs (list fsl-base))
    (inputs (list lapack
                  zlib))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:make-flags
           #~(list (string-append "CC=" (search-input-file %build-inputs "bin/gcc")))
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Utility library for FSL")
    (description "This package is a utility for FSL.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/znzlib")
    (license fsl-license)))

(define-public fsl-newnifti
  (package
    (name "fsl-newnifti")
    (version "4.1.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/NewNifti")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0b9843cjk4qg24p8z19iwzsn40y4hqc4x2k8619wrkg6b6c3x227"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-znzlib
                  zlib
                  lapack))
    (arguments
     (list #:tests? #f ; No check target in Makefile
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Encapsulation of the reference implementation of the NIFTI format")
    (description "A library that encapsulates the reference implementation
of the NIfTI format as well as the legacy code
necessary to perform I/O operations on NIfTI files.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/NewNifti")
    (license fsl-license)))

(define-public fsl-cprob
  (package
    (name "fsl-cprob")
    (version "2111.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/cprob")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1m7cpb6i6j3cgn21aqzqicl1xw3l65yvy11f3i7668c8k5pdijf2"))))
    (native-inputs (list fsl-base))
    (inputs (list lapack
                  zlib))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Wrapper around the CEPHES cmath library")
    (description "A wrapper that encapsulates the CEPHES Cmath library.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/cprob")
    (license fsl-license)))

(define-public fsl-miscmaths
  (package
    (name "fsl-miscmaths")
    (version "2203.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/miscmaths")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1maamfvs5ivb1a726g8qm8bvzb92bgfizcibp7mhp4hx9jllwhva"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-newnifti
                  fsl-armawrap
                  fsl-utils
                  fsl-znzlib
                  fsl-cprob
                  zlib
                  lapack))
    (arguments
     (list #:tests? #f ; No tests.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Implementation of miscellaneous mathematicals methods")
    (description "A library that implement miscellaneous mathematicals methods.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/miscmaths")
    (license fsl-license)))

(define-public fsl-newimage
  (package
    (name "fsl-newimage")
    (version "2203.12")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/newimage")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0yy2z2wb5glkizqlqw1fbxq243mp90igfnpd89xfmxqwnpwx7hcs"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-newnifti
                  fsl-znzlib
                  zlib
                  fsl-miscmaths
                  fsl-utils
                  boost
                  fsl-cprob
                  lapack))
    (arguments
     (list #:tests? #f ; No check phase in Makefile.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Implementation of a data container for nifti images/volumes")
    (description "A library that returns the object representing an image/volume
once it has been deserialized by the newnifti library.
The library implements a data container and common operations for nifti images/volumes")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/newimage")
    (license fsl-license)))

(define-public fsl-avwutils
  (package
    (name "fsl-avwutils")
    (version "2209.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/avwutils.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1wfzskp3pc677rv6vhvqrxr1ax0qz27n5ai3a75chlgjc1901q99"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-newimage
                  fsl-newnifti
                  fsl-armawrap
                  fsl-znzlib
                  fsl-miscmaths
                  fsl-utils
                  fsl-cprob
                  lapack
                  nlohmann-json
                  zlib))
    (arguments
     (list #:tests? #f                  ; No check target in Makefile.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Miscellaneous image utilities")
    (description "It is a set of useful command-line utilities which allow the conversion,
processing etc.of Analyze/AVW and Nifti format data sets")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/avwutils.git")
    (license fsl-license)))

(define-public fsl-verbena
  (package
    (name "fsl-verbena")
    (version "v4.0.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/physimals/verbena")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0nn9zj4r45512am4rxz60pn5bkzd9x123g1psqa3vavmsv48iklj"))))
    (native-inputs (list fsl-base))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
      #:phases
      #~(modify-phases %standard-phases
          (delete 'configure)
          (add-before 'build 'extra-env
            (lambda _
              (setenv "FSLCONFDIR"
                      (string-append #$(this-package-native-input "fsl-base") "/config"))
              (setenv "FSLDEVDIR" #$output))))))
     (build-system gnu-build-system)
     (synopsis "Vascular Model Based Perfusion Quantification for DSC-MRI")
     (description "Verbena is a Bayesian Inference tool for quantification
of perfusion and other haemodynamic parameters
from Dynamic Susceptibility Contrast perfusion MRI of the brain.")
     (home-page "https://github.com/physimals/verbena")
     (license (license:non-copyleft
               "https://github.com/physimals/verbena/blob/v4.0.3/LICENSE"))))

(define-public fsl-basisfield
  (package
    (name "fsl-basisfield")
    (version "2203.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/basisfield.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "10fpd60w6ac1mp1nj1bdih7fh71bnnyilfgn8kh3sa2g3j1jmf7x"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-miscmaths
                  fsl-newimage
                  fsl-newnifti
                  fsl-utils
                  fsl-znzlib
                  lapack
                  zlib))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
     (build-system gnu-build-system)
     (synopsis "Basisfield models and corrects B0 field inhomogeneities in MRI")
     (description "Basisfield is a tool within the FSL (FMRIB Software Library)
used to model and correct B0 field inhomogeneities in MRI images,
which can cause geometric distortions.
It is essential for improving image quality and precision
in both clinical and research applications.")
     (home-page "https://git.fmrib.ox.ac.uk/fsl/basisfield")
     (license fsl-license)))

(define-public fsl-cudabasisfield
  (package
    (name "fsl-cudabasisfield")
    (version "1.1.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/cudabasisfield.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1hay9x050vmn4cc9w3zng5c578w4xsgidhh2m8yk0nq0jwab9s9g"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-basisfield
                  fsl-miscmaths
                  fsl-newimage
                  fsl-newnifti
                  fsl-utils
                  fsl-znzlib
                  cuda-11.7
                  lapack
                  zlib))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:validate-runpath? #f
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output)
                   (setenv "NVCC" (string-append #$(this-package-input "cuda-toolkit")
                                                 "/bin/nvcc"))
                   (setenv "CUDA_VER" #$(version-major+minor
                                         (package-version
                                          (this-package-input "cuda-toolkit")))))))))
    (build-system gnu-build-system)
    (synopsis "CUDA helper functions for GPU acceleration in FSL")
    (description "cudabasisfield is a FSL module providing helper functions for CUDA
operations, facilitating GPU acceleration in FSL tools.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/cudabasisfield")
    (license fsl-license)))

(define-public fsl-meshclass
  (package
    (name "fsl-meshclass")
    (version "2111.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/meshclass.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1kq26f8qz1s4rw0k7k9n5igqcr6dw39h3vyb4f1gi3g245al1fh0"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-cprob
                  fsl-miscmaths
                  fsl-newimage
                  fsl-newnifti
                  fsl-utils
                  fsl-znzlib
                  lapack
                  zlib))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
     (build-system gnu-build-system)
     (synopsis "Represents 3D models for geometric operations")
     (description "The meshclass module in FSL provides
the Mesh class for representing and manipulating 3D models made
of triangles, supporting various geometric operations and querie")
     (home-page "https://git.fmrib.ox.ac.uk/fsl/meshclass")
     (license fsl-license)))

(define-public fsl-bet2
  (package
    (name "fsl-bet2")
    (version "2111.8")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/bet2.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0v705hf273cimm5sh8z372rizj9lz5gfs0alv776f2r7mrzrh8h5"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-cprob
                  fsl-meshclass
                  fsl-miscmaths
                  fsl-newimage
                  fsl-newnifti
                  fsl-utils
                  fsl-znzlib
                  lapack
                  zlib))
    (arguments
     (list #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (replace 'check
                 (lambda* (#:key tests? #:allow-other-keys)
                   (when tests?
                     (chmod "bet4animal" #o755)
                     (substitute* "tests/feedsRun.launchB4A"
                       (("bet4animal") (string-append (getcwd) "/bet4animal")))
                     (invoke "./tests/feedsRun.launchB4A"))))
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Removes non-brain tissue from MRI images")
    (description "The bet2 module in FSL is a brain extraction tool
that removes non-brain tissue from MRI images,
aiding in isolating the brain for further analysis.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/bet2")
    (license fsl-license)))

(define-public fsl-fast4
  (package
    (name "fsl-fast4")
    (version "2111.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/fast4.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0a0yby2d16a1s335psv7b5frmpfw894jywj2iahs9g6v323spyx6"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-cprob
                  fsl-miscmaths
                  fsl-newimage
                  fsl-newnifti
                  fsl-utils
                  fsl-znzlib
                  lapack
                  zlib))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Segments brain images and corrects bias field inhomogeneities")
    (description "FAST4 is an automated segmentation tool in FSL that segments brain
images into different tissue types and corrects for bias field
inhomogeneities.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/fast4")
    (license fsl-license)))

(define-public fsl-warpfns
  (package
    (name "fsl-warpfns")
    (version "2203.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://git.fmrib.ox.ac.uk/fsl/warpfns.git")
              (commit version)))
        (file-name (git-file-name name version))
        (sha256
        (base32 "1kbdkvzfhml18sx5vbrvnbc2lq05m35n9jvygfql01m67k5b3ps0"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-basisfield
                  fsl-cprob
                  fsl-meshclass
                  fsl-miscmaths
                  fsl-newimage
                  fsl-newnifti
                  fsl-utils
                  fsl-znzlib
                  lapack
                  zlib))
    (arguments
      (list #:tests? #f ; No check target in Makefile.
            #:phases
            #~(modify-phases %standard-phases
                (delete 'configure)
                (add-before 'build 'extra-env
                  (lambda _
                    (setenv "FSLCONFDIR"
                            (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                    (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Warp functions for FSL")
    (description "Warpfns is a set of functions within the FSL (FMRIB Software Library)
used for handling and applying spatial transformations (warps) to MRI
images.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/warpfns")
    (license fsl-license)))

(define-public fsl-flirt
  (package
    (name "fsl-flirt")
    (version "2111.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/flirt.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1gj37s8yls885vm5dhcvkfyypbm54fl3b96qx1s5kwyrxjjm7bbm"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-basisfield
                  fsl-cprob
                  fsl-meshclass
                  fsl-miscmaths
                  fsl-newnifti
                  fsl-newimage
                  fsl-utils
                  fsl-warpfns
                  fsl-znzlib
                  lapack
                  zlib))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Linear Image Registration Tool")
    (description "FLIRT (FMRIB's Linear Image Registration Tool) is a fully automated
robust and accurate tool for linear (affine) intra- and inter-modal
brain image registration.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/flirt")
    (license fsl-license)))

(define-public fsl-mcflirt
  (package
    (name "fsl-mcflirt")
    (version "2111.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/mcflirt.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "14afvc3k36kir1md1nb01c2ndqpqczbpvzjvswrkwf3gx59ckd19"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-cprob
                  fsl-miscmaths
                  fsl-newimage
                  fsl-newnifti
                  fsl-utils
                  fsl-znzlib
                  lapack
                  zlib))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Motion correction tool for fMRI data")
    (description "MCFLIRT (Motion Correction using FMRIB's Linear Image Registration
Tool) is a tool within FSL designed to perform motion correction on
fMRI data.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/mcflirt")
    (license fsl-license)))

(define-public fsl-fnirt
  (package
    (name "fsl-fnirt")
    (version "2203.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://git.fmrib.ox.ac.uk/fsl/fnirt.git")
              (commit version)))
        (file-name (git-file-name name version))
        (sha256
        (base32 "1dv4c7k1a4a5r71vxw3bwahxxhvfkbyr8189d87yjjny55gv35ni"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-basisfield
                  fsl-cprob
                  fsl-meshclass
                  fsl-miscmaths
                  fsl-newimage
                  fsl-newnifti
                  fsl-utils
                  fsl-warpfns
                  fsl-znzlib
                  lapack
                  zlib))
    (arguments
      (list #:tests? #f ; No check target in Makefile.
            #:phases
            #~(modify-phases %standard-phases
                (delete 'configure)
                (add-before 'build 'extra-env
                  (lambda _
                    (setenv "FSLCONFDIR"
                            (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                    (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Non-linear Image Registration Tool")
    (description "FNIRT (FMRIB's Non-linear Image Registration Tool) is a tool for
non-linear registration of brain images, used to align images from
different subjects or modalities.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/fnirt")
    (license fsl-license)))

(define-public fsl-topup
  (package
    (name "fsl-topup")
    (version "2203.5")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://git.fmrib.ox.ac.uk/fsl/topup.git")
              (commit version)))
        (file-name (git-file-name name version))
        (sha256
        (base32 "0s45z91gwqp30j1cjjjiyi8iq9avkc67d8mb9pq41dqp5lag87r0"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-basisfield
                  fsl-cprob
                  fsl-miscmaths
                  fsl-meshclass
                  fsl-newimage
                  fsl-newnifti
                  fsl-utils
                  fsl-warpfns
                  fsl-znzlib
                  lapack
                  zlib))
    (arguments
      (list #:tests? #f ; No check target in Makefile.
            #:phases
            #~(modify-phases %standard-phases
                (delete 'configure)
                (add-before 'build 'extra-env
                  (lambda _
                    (mkdir-p #$output)
                    (setenv "FSLCONFDIR"
                            (string-append
                             #$(this-package-native-input "fsl-base") "/config"))
                    (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Corrects susceptibility-induced distortions in EPI data")
    (description "Topup is a tool within FSL that corrects for susceptibility-induced
distortions in echo planar imaging (EPI) data.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/topup")
    (license fsl-license)))

(define-public fsl-eddy
  (package
    (name "fsl-eddy")
    (version "2401.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/eddy.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1lxwas8c4brw763x8x706fz7ax0cfhz8ds32l88c4s1zarl30jy2"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-basisfield
                  fsl-cprob
                  fsl-miscmaths
                  fsl-meshclass
                  fsl-newimage
                  fsl-newnifti
                  fsl-topup
                  fsl-utils
                  fsl-warpfns
                  fsl-znzlib
                  boost
                  lapack
                  nlohmann-json
                  zlib))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Corrects eddy current distortions in diffusion MRI data")
    (description "Eddy is a tool within FSL that corrects for eddy current distortions
and head movements in diffusion MRI data.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/eddy")
    (license fsl-license)))

(define-public fsl-eddy-cuda
  (package/inherit fsl-eddy
    (name "fsl-eddy-cuda")
    (inputs (modify-inputs (package-inputs fsl-eddy)
              (append cuda-11.7 fsl-cudabasisfield)))
    (arguments
     (substitute-keyword-arguments (package-arguments fsl-eddy)
       ((#:phases phases '%standard-phases)
        #~(modify-phases #$phases
            (add-before 'build 'set-nvcc
              (lambda _
                (setenv "NVCC" (string-append #$(this-package-input "cuda-toolkit")
                                              "/bin/nvcc"))))
            ;; The Makefile doesn't honour MAKEFLAGS.
            (replace 'build
              (lambda _
                (invoke "make" "cuda=1")))
            (replace 'install
              (lambda _
                (invoke "make" "cuda=1" "install")))))
       ((#:validate-runpath? #t #t)
        #f)))
    (synopsis "Corrects eddy current distortions in diffusion MRI data (CUDA version)")))

(define-public fsl-libgdc
  (package
    (name "fsl-libgdc")
    (version "2111.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/libgdc.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1zn0r5z619n4fj0w3w2k5da25fl058zr1bxcn4i22k4nf08yq2qj"))))
    (native-inputs (list fsl-base))
    (inputs (list gd
                  lapack
                  libpng))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:make-flags
           #~(list (string-append "CC=" (search-input-file %build-inputs "bin/gcc")))
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Essential graphical and data handling module in FSL")
    (description "libgdc is a module within FSL that provides essential graphical or
data handling functionalities, with its source code available on
GitLab.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/libgdc")
    (license fsl-license)))

(define-public fsl-libvis
  (package
    (name "fsl-libvis")
    (version "2111.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/libvis.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "18irnxklsrqx1h040kqpyp5fm7y1f4nz61g3hi9jzk8pcnnz2z1s"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-cprob
                  fsl-libgdc
                  fsl-miscmaths
                  fsl-newimage
                  fsl-newnifti
                  fsl-utils
                  fsl-znzlib
                  gd
                  lapack
                  libpng
                  zlib))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:make-flags
           #~(list (string-append "CC=" (search-input-file %build-inputs "bin/gcc")))
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (mkdir-p #$output)
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output)))
               ;; Target dependency graph is not properly defined in
               ;; the Makefile, so the following targets have to be
               ;; built before the "all" target.
               (add-after 'extra-env 'pre-build
                 (lambda _
                   (invoke "make" "libfsl-miscplot.so" "libfsl-miscpic.so"))))))
    (build-system gnu-build-system)
    (synopsis "Miscellaneous low-level visualisation scripts and utilities")
    (description "Module within FSL that provides visualization functionalities, with
its source code available on GitLab.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/libvis")
    (license fsl-license)))

(define-public fsl-feat5
  (package
    (name "fsl-feat5")
    (version "2201.7")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/feat5.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "13rcfcczip24nw8ylb3f2qzmwfnm93wydkss6iay9x34459jrjqg"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-cprob
                  fsl-libgdc
                  fsl-libvis
                  fsl-miscmaths
                  fsl-newimage
                  fsl-newnifti
                  fsl-utils
                  fsl-znzlib
                  gd
                  lapack
                  libpng
                  zlib))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Advanced FEAT module for fMRI data analysis")
    (description "Module within FSL that provides advanced functionalities for
FEAT (FMRI Expert Analysis Tool), including pre-processing steps,
model setup, and statistical analysis for fMRI data.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/feat5")
    (license fsl-license)))

(define-public fsl-possum
  (package
    (name "fsl-possum")
    (version "2111.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/possum.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "11vpr0ds0if9ib868cm3wnfvaaacfbk7v4hh80v5g6zw50g5rd45"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-cprob
                  fsl-miscmaths
                  fsl-newimage
                  fsl-newnifti
                  fsl-utils
                  fsl-znzlib
                  lapack
                  zlib))
    (arguments
     (list #:tests? #f  ; No check target in Makefile.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Advanced MRI simulation module in FSL")
    (description "Module within FSL that provides advanced functionalities for
simulating MRI data, including pulse sequence generation, signal
generation, noise addition, and image reconstruction.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/possum")
    (license fsl-license)))

(define-public fsl-susan
  (package
    (name "fsl-susan")
    (version "2111.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/susan.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "14nmpjlcc64f3d4wxvcy6hnzm8wvvjx35pv0zrrf0hq97ggdi4jr"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-cprob
                  fsl-miscmaths
                  fsl-newimage
                  fsl-newnifti
                  fsl-utils
                  fsl-znzlib
                  lapack
                  zlib))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Reduces noise in MRI data using nonlinear filtering")
    (description "Module within FSL that provides advanced functionalities for noise
reduction in MRI data using nonlinear filtering techniques.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/susan")
    (license fsl-license)))

(define-public fsl-firstlib
  (package
    (name "fsl-firstlib")
    (version "2111.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/first_lib.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1wsyqwgc6jgyxw0lkjgpz68zpnmbp0xcc4fczr525a1y6pva6s64"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-miscmaths
                  fsl-newimage
                  fsl-newnifti
                  fsl-utils
                  fsl-znzlib
                  lapack
                  zlib))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Support library for brain segmentation")
    (description "Library or collection of resources supporting FIRST, FSL's tool for
automated segmentation of subcortical brain structures, enhancing its
functionality and accuracy.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/first_lib")
    (license fsl-license)))

(define-public fsl-vtkio
  (package
    (name "fsl-vtkio")
    (version "2111.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/fslvtkio.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "09armk22hwfh2xp15wxjiprgcglzd0s6d6azdqg2h7klf85i4r03"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-firstlib
                  fsl-meshclass
                  fsl-miscmaths
                  fsl-newimage
                  fsl-newnifti
                  fsl-utils
                  fsl-znzlib
                  lapack
                  zlib))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "VTK-based I/O library for neuroimaging")
    (description "Module within FSL that facilitates input/output operations using
VTK (Visualization Toolkit), enabling advanced visualization and data
handling for neuroimaging.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/fslvtkio")
    (license fsl-license)))

(define-public fsl-shapemodel
  (package
    (name "fsl-shapemodel")
    (version "2111.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/shapeModel.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "02yqd4m1yzd6n5wq93nb70n3zxsgmypcl1q5jramfiilkmf0y6p7"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-firstlib
                  fsl-meshclass
                  fsl-miscmaths
                  fsl-newimage
                  fsl-newnifti
                  fsl-utils
                  fsl-vtkio
                  fsl-znzlib
                  lapack
                  zlib))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Shape modeling tool for neuroimaging")
    (description "Library within FSL for creating and analyzing shape models of brain
structures, aiding in morphometric studies and neuroimaging research.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/shapeModel")
    (license fsl-license)))

(define-public fsl-first
  (package
    (name "fsl-first")
    (version "2203.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/first.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1ip610yg556rv49mbgwmjmhm6s06j01ipb8mvfxsbfpfzza3cbdj"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-cprob
                  fsl-firstlib
                  fsl-meshclass
                  fsl-miscmaths
                  fsl-newimage
                  fsl-newnifti
                  fsl-shapemodel
                  fsl-utils
                  fsl-vtkio
                  fsl-znzlib
                  lapack
                  zlib))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "Automated brain structure segmentation tool")
    (description "Tool within FSL for automated segmentation of subcortical brain
structures from MRI images, aiding neuroscience research.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/first")
    (license fsl-license)))

(define-public fsl-fugue
  (package
    (name "fsl-fugue")
    (version "2201.5")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.fmrib.ox.ac.uk/fsl/fugue.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1xpf1iafy1k417py03jkzmcqfhmblbal2yp0ywm3j2b3b5av78s3"))))
    (native-inputs (list fsl-base))
    (inputs (list fsl-armawrap
                  fsl-basisfield
                  fsl-cprob
                  fsl-meshclass
                  fsl-miscmaths
                  fsl-newimage
                  fsl-newnifti
                  fsl-utils
                  fsl-warpfns
                  fsl-znzlib
                  lapack
                  zlib))
    (arguments
     (list #:tests? #f ; No check target in Makefile.
           #:phases
           #~(modify-phases %standard-phases
               (delete 'configure)
               (add-before 'build 'extra-env
                 (lambda _
                   (setenv "FSLCONFDIR"
                           (string-append
                            #$(this-package-native-input "fsl-base") "/config"))
                   (setenv "FSLDEVDIR" #$output))))))
    (build-system gnu-build-system)
    (synopsis "EPI distortion correction tool")
    (description "Tool within FSL designed for fieldmap-based correction of geometric
distortions in EPI images, crucial for accurate fMRI analysis.")
    (home-page "https://git.fmrib.ox.ac.uk/fsl/fugue")
    (license fsl-license)))
